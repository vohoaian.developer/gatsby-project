# Gatsby

## CLI

```
npm i -g gatsby-cli
gatsby --help
```

### `new`

```
gatsby new my-gatsby
```

### `develop`

```
cd my-gatsby
# gatsby develop
gatsby develop -H 0.0.0.0
```

## Settings

### `gatsby-config.js`

```js
module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    siteUrl: `https://gatsbystarterdefaultsource.gatsbyjs.io/`,
  },
  plugins: [
    // plugins here
  ],
}
```

## Pages and Components

### Pages

Defined with a name in the form of `src/pages/*.js`

```jsx
// about.js
import React from "react"

export default function About() {
  return (
    <div>
      <h1>About us</h1>
      <p>Welcome to our about page! Here is some info about us.</p>
    </div>
  )
}
```

### (React) Components

Defined with a name in the form of `src/components/*.js`

```jsx
import React from "react"

export default function Footer() {
  return <p>&copy; 2022 My Gatsby Site. All rights reserved.</p>
}
```

### Linking

```jsx
import { Link } from "gatsby"

export default function About() {
  return (
    // ...
    <Link to="/">Home</Link>
    // ...
  )
}
```

### The `Layout` component

`src/components/layout.js`

```jsx
/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"

const Layout = ({ children }) => {
  return (
    <>
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>
      </div>
    </>
  )
}

export default Layout
```

## Using CSS

### Global styling

#### With `layout`

```css
/* src/components/layout.css */
div {
  margin: 0 auto;
  max-width: 960px;
  padding: 0 1.5rem;
}
```

```js
// layout.js
import "./layout.css"
// ...
```

#### Without `layout`

```css
/* src/styles/global.css */
div {
  margin: 0 auto;
  max-width: 960px;
  padding: 0 1.5rem;
}
```

```js
// gatsby-browser.js
import "./src/styles/global.css"
// require('./src/styles/global.css')
```

#### With component stylesheets (still requrie global namespaces)

```css
/* src/components/footer.css */
div {
  margin: 0 auto;
  max-width: 960px;
  padding: 0 1.5rem;
}
```

```js
// footer.js
import "./fotter.css"
//...
```

### Modular styling

```css
/* src/components/footer.module.css */
.footer {
  margin: 0 auto;
  max-width: 960px;
  padding: 0 0.1rem;
  background: blue;
}
```

```jsx
// footer.js
import React from "react"
import * as footerStyles from "./footer.module.css"

export default function Footer() {
  return (
    <p className={footerStyles.footer}>
      &copy; 2022 My Gatsby Site. All rights reserved.
    </p>
  )
}
```

### CSS-in-JS

- Component-driven
- Spoced locally
- Styled dynamically
- Performance-tuned

#### Emotion

```
yarn install gatsby-plugin-emotion @emotion/react @emotion/styled
```

```js
// gatsby-config.js
module.exports = {
  plugins: ['gatsby-plugin-emotion],
}
```

```jsx
// ...
import styled from "@emotion/styled"
import { css } from "@emotion/react"

const Header = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 0 1.5rem;
  background-color: blue;
  color: white;
`

export default function Home() {
  return (
    <Header>
      {
        // ...
      }
    </Header>
  )
}
```

#### Styled components

```
yarn install gatsby-plugin-styled-components styled-components babel-plugin-styled-components
```

```js
// gatsby-config.js
module.exports = {
  plugins: ["gatsby-plugin-styled-components"],
}
```

```jsx
// src/page/index.js
// ...
import styled from "styled-components"

const Header = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 0 1.5rem;
  background-color: blue;
  color: white;
`

// ...
```

##### GlobalStyle

```jsx
// layout.js
// ...
import { createGlobalStyle } from "styled-components"

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${props => (props.theme === "blue" ? "blue" : "white")};
  }
`

export default function Layout({ children }) {
  return (
    <React.Fragment>
      <GlobalStyle theme="blue" />
    </React.Fragment>
  )
}
```

## Plugins

- Source plugins
- Transformer plugins
- External service plugins
  [Gatsby Plugin Library](https://www.gatsbyjs.com/plugins)

### Installing

```
yarn install gatsby-plugin-google-analytics
```

```js
// gatsby-config.js
module.exports = {
  plugins: ["gatsby-plugin-google-analytics"],
}
```

Some plugins also accept options in their definitions:

```js
// gatsby-config.js
module.exports = {
  plugins: [
    {
      resolve: "gatsby-plugin-google-analytics",
      options: {
        trackingId: "TRACKING_ID",
        head: false,
        anonymize: true,
        respectDNT: true,
        exclude: ["/preview/**", "do-not-track/me/too"],
        pageTransitionDelay: 0,
        optimizeId: "OPTIMIZE_TRACKING_ID",
        experimentId: "EXPERIMENT_ID",
        variationId: "VARIATION_ID",
        defer: false,
        sampleRate: 5,
        siteSpeedSampleRate: 10,
        cookieDomain: "example.com",
      },
    },
  ],
}
```

### Loading local plugins

Let's create a folder named `plugins/gatsby-local-plugin`

With local plugins, you don't need an NPM installation command, but you need to configure the plugin in `gatsby-config.js` so that Gatsby is aware of it

```js
// gatsby-config.js
module.exports = {
  plugins: ['gatsby-local-plugin`]
}
```

To verify your plugin is loading correctly in your Gatsby site, you can add a `gatsby-node.js` file to the plugin directory.

```js
// plugins/gatsby-local-plugin/gatsby-node.js
exports.onPreInit = () => {
  console.log("Hey, I'm a local plugin that just got loaded! Look at me!")
}
```
